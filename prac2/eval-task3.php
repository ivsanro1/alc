<?php
function evalTask3($qrel, $run, &$result) {
  $QREL = array();
  $DATA = array();
  $CONFUSION = array();

  if(!($fd = fopen($qrel, 'r'))) {
    echo 'Cannot open qrel file.'."\n";
    return false;
  }
  $qrel_size = 0;
  while(!feof($fd)) {
    $line = trim(fgets($fd), "\n\r");
    if(preg_match('/^(.+?)\t(.+?)\t(.+?)$/', $line, $fields)) {
      $qrel_size++;
      $id = $fields[1];
      $value = $fields[2];
      if(!isset($QREL[$id]))
        $QREL[$id] = array($value => 1);
      else
        $QREL[$id][$value] = 1;
    } elseif(!empty($line)) {
      echo 'Wrong qrel file format: "'.$line.'".'."\n";
      return false;
    }
  }
  fclose($fd);
  foreach($QREL as $id => &$valuesqrel)
    ksort($valuesqrel);
  if(!($fd = fopen($run, 'r'))) {
    echo 'Cannot open run file.'."\n";
    exit(1);
  }
  while(!feof($fd)) {
    $line = trim(fgets($fd), "\n\r");
    if(preg_match('/^([^\t]+?)\t?$/', $line, $fields)) {
      $id = $fields[1];
      if(isset($QREL[$id]))
        ;
      elseif($checkdata) {
        echo 'Unknown instance id: "'.$id.'".'."\n";
        return false;
      }
    } elseif(preg_match('/^(.+?)\t(.+?)$/', $line, $fields)) {
      $id = $fields[1];
      $value = $fields[2];
      if(isset($QREL[$id])) {
        if(!isset($DATA[$id]))
          $DATA[$id] = array($value => 1);
        else
          $DATA[$id][$value] = 1;
      } else {
        echo 'Unknown instance id: "'.$id.'".'."\n";
        return false;
      }
    } elseif(!empty($line)) {
      echo 'Wrong run file format: "'.$line.'".'."\n";
      return false;
    }
  }
  fclose($fd);

  $tp = 0;
  $fp = 0;
  $fn = 0;
  foreach($QREL as $id => $valuesqrel) {
    $all = implode('|', array_keys($valuesqrel));
    if(isset($DATA[$id])) {
      foreach($DATA[$id] as $k => $v) {
        if(isset($valuesqrel[$k])) {
          $tp++;
          unset($valuesqrel[$k]);
        } else
          $fp++;
        if(!isset($CONFUSION[$k]))
          $CONFUSION[$k] = array($all => 1);
        elseif(!isset($CONFUSION[$k][$all]))
          $CONFUSION[$k][$all] = 1;
        else
          $CONFUSION[$k][$all]++;
      }
      if(!empty($valuesqrel))
        $fn += sizeof(array_keys($valuesqrel));
    } else {
     if(!isset($CONFUSION['?']))
        $CONFUSION['?'] = array($all => 1);
      elseif(!isset($CONFUSION['?'][$all]))
        $CONFUSION['?'][$all] = 1;
      else
        $CONFUSION['?'][$all]++;
    }
  }
  $p = $tp/($tp+$fp);
  $r = $tp/($tp+$fn);
  if($p+$r==0)
    $f1 = 0;
  else
    $f1 = 2*$p*$r/($p+$r);
  $result = array('p' => $p,
                  'r' => $r,
                  'f1' => $f1,
                  'info' => '');
  $result['info'] = sizeof($QREL).' instances in qrel, '.$qrel_size.' annotations in qrel, '.($tp+$fp).' predictions.'."\n".
                    "\n".
                    'Microaveraged Precision: '.number_format($p, 3)."\n".
                    'Microaveraged Recall: '.number_format($r, 3)."\n".
                    'Microaveraged F1: '.number_format($f1, 3)."\n".
                    "\n".
                    'Confusion matrix (exact match)'."\n".
                    'Pred'."\t".'Actual'."\t".'#'."\n";
  ksort($CONFUSION);
  foreach($CONFUSION as $k1 => $v1) {
    foreach($v1 as $k2 => $v2)
      $result['info'] .= $k1."\t".$k2."\t".$v2."\n";
  }
  return $result;
} // evalTask3

if(isset($argv)) {
  if(sizeof($argv)-1!=2) {
    echo 'Usage: php eval-task3.php <qrel_file> <run_file>'."\n";
    exit(1);
  }
  if(evalTask3($argv[1], $argv[2], $result))
    print_r($result);
}
?>

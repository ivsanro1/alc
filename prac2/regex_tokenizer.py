#!/usr/bin/env python
# -*- coding: utf-8 -*-

#
# Iván Sánchez Rodríguez
# ALC - MIARFID  2017
#

import re
import argparse

def parse_args():
    parser = argparse.ArgumentParser(description='Tokenizes the text within a text file. Iván Sánchez Rodríguez. ALC  -  MIARFID 2017')
    parser.add_argument('input_path', metavar='input',
                        help='relative path to the input text file')
    parser.add_argument('output_path', metavar='output', const='output.txt', nargs='?',
                        help='relative path to the output text file. If there is no output specified, the result will be printed instead of saved')


    return parser.parse_args()


if __name__ == "__main__":
    #args = parse_args()

    #with open(args.input_path, 'r') as myfile:
    #    text=myfile.read()



    #pattern_messib = '([\#\@]?l[ei]o(nel)?\S*)? (\s)* ([\#\@]?me(s)?si\S*)?'
    aspects = [
                'Afición',
                'Arbitro'
                'Autoridades'
                'Entrenador'
                'Equipo-Atlético_de_Madrid'
                'Equipo-Barcelona'
                'Equipo-Real_Madrid'
                'Equipo',
                'Jugador-Alexis_Sánchez'
                'Jugador-Alvaro_Arbeloa'
                'Jugador-Andrés_Iniesta'
                'Jugador-Angel_Di_María'
                'Jugador-Asier_Ilarramendi'
                'Jugador-Carles_Puyol'
                'Jugador-Cesc_Fábregas'
                'Jugador-Cristiano_Ronaldo'
                'Jugador-Dani_Alves'
                'Jugador-Dani_Carvajal'
                'Jugador-Fábio_Coentrão'
                'Jugador-Gareth_Bale'
                'Jugador-Iker_Casillas'
                'Jugador-Isco'
                'Jugador-Javier_Mascherano'
                'Jugador-Jesé_Rodríguez'
                'Jugador-José_Manuel_Pinto'
                'Jugador-Karim_Benzema'
                'Jugador-Lionel_Messi'
                'Jugador-Luka_Modric'
                'Jugador-Marc_Bartra'
                'Jugador-Neymar_Jr.'
                'Jugador-Pedro_Rodríguez'
                'Jugador-Pepe'
                'Jugador-Sergio_Busquets'
                'Jugador-Sergio_Ramos'
                'Jugador-Xabi_Alonso'
                'Jugador-Xavi_Hernández'
                'Jugador'
                'Partido'
                'Retransmisión'
                ]

    regex = {}

    regex['Afición'] = u'(afici[óo]n)|(?:anti)?(madridistas?)|(merengu?es?)|(bar[sc]elonistas?)|(fan[áa]ticos?)|(cibeles)|(catal[áa]n(?:es)?)|(cul[ée]s?)|(segu?idor(?:es)?)'
    regex['Arbitro'] = u'(mateu\s*lahoz)|(mateu)|(lahoz)|(penal)|(expulsi[oó]n)|([áa][rl]bitro)|([áa][rl]bitr\S*)'
    regex['Autoridades'] = u'(presidentes?)|(dirigentes?)|(aguirre)|(directivas?)|(florentino)|(cifuentes)|(wert)|(re[yi])|(polic[íi]as?)'
    regex['Entrenador'] = u'(martino)|(tito)|(tata)|(mou(?:ri(?:ñ|(?:nh))o)?)|(pep\s*guardiola)|(pep)|(puardiola)|(ancelotti)\(entrenador)'
    regex['Equipo-Atlético_de_Madrid'] = u'(atl[ée]ti)'
    regex['Equipo-Barcelona'] = u'(bar[scç]a)|(bar[sc]elona)'
    regex['Equipo-Real_Madrid'] = u'(real\s*madri[dz])|(madri[dz])'
    regex['Equipo'] = u'(bayern)|(valencia)|(manchester)|(levante)|(liverpool)|(arsenal)|(mirand[ée]s)|(Boruss?ia\s(?:de)\sDort?mund)|(Boruss?ia\sDort?mund)|(Boruss?ia)|(Dort?mund)|(e(?:k|qu?)ip(?:o|azo)s?)'
    regex['Jugador-Alexis_Sánchez'] = u'(alexis)'
    regex['Jugador-Alvaro_Arbeloa'] = u'(arbeloa)|(cono[\s,\.])'
    regex['Jugador-Andrés_Iniesta'] = u'(iniesta)'
    regex['Jugador-Angel_Di_María'] = u'(di\s?mar[íi]a)'
    regex['Jugador-Asier_Ilarramendi'] = u'(asier\sill?arra)|(ill?arra)|(asier)'
    regex['Jugador-Carles_Puyol'] = u'(pu[jy]ol)'
    regex['Jugador-Cesc_Fábregas'] = u'(cesc\sf[áa]bregas)|(cesc)|(f[áa]bregas)'
    regex['Jugador-Cristiano_Ronaldo'] = u'(cristiano\sronaldo)|(cristiano)|(ronaldo)|(cr7)'
    regex['Jugador-Dani_Alves'] = u'(alves)'
    regex['Jugador-Dani_Carvajal'] = u'(carvajal)'
    regex['Jugador-Fábio_Coentrão'] = u'(Coentr[aã]o)'
    regex['Jugador-Gareth_Bale'] = u'(bale)'
    regex['Jugador-Iker_Casillas'] = u'([íi]ker\scasillas)|([íi]ker)|(casillas)'
    regex['Jugador-Isco'] = u'isco'
    regex['Jugador-Javier_Mascherano'] = u'(masche(?:rano)?)'
    regex['Jugador-Jesé_Rodríguez'] = u'(jess?[eé])'
    regex['Jugador-José_Manuel_Pinto'] = u'pinto'
    regex['Jugador-Karim_Benzema'] = u'(karim\sbenzem[áa])|(karim)|(benzem[áa])'
    regex['Jugador-Lionel_Messi'] = u'([\#\@]?l[ei]o(?:nel)?\S*)\s*([\#\@]?me(?:s)?si\S*)|([\#\@]?l[ei]o(?:nel)?\S*)|([\#\@]?me(?:s)?si\S*)'
    regex['Jugador-Luka_Modric'] = u'(modric)'
    regex['Jugador-Marc_Bartra'] = u'(bartra)'
    regex['Jugador-Neymar_Jr.'] = u'(neymar)'
    regex['Jugador-Pedro_Rodríguez'] = u'(pedr)'
    regex['Jugador-Pepe'] = u'(pepe)'
    regex['Jugador-Sergio_Busquets'] = u'(busquets)'
    regex['Jugador-Sergio_Ramos'] = u'(ramos)'
    regex['Jugador-Xabi_Alonso'] = u'(xabi\salonso)|(xabi)|(alonso)'
    regex['Jugador-Xavi_Hernández'] = u'(xavi\shern[áa]ndez)|(xavi)|(hern[áa]ndez)'
    regex['Jugador'] = u'(jugador(?:es)?)|(vald[ée]s)|(suplente)|(arquero)|(MVP)|(capit[aá]n)|(defensa)|(titular)|(plantilla)|(payasos)|(tramposos)|(piscineros)|(portero)|(ataque)|(santos)|(song)|(Butragueño)|([öo]zil)|(montoya)|(t[eé]vez)|(marcelo)|(piqu[eé])|(fichaje)'
    regex['Partido'] = u'(copa\sdel\srey)|(liga)|(champions)|(copa)|(partidos?)'
    regex['Retransmisión'] = u'(en\sdirecto)|(v[íi]deo)|(FCBtv)|(tv)|(televisi[oó]n|(foto)|(20minutos)|(grabar)|(grab[óo])|(marca)|(radio)'








    #tokens = re.findall(pattern_date+'|'+pattern_time+'|'+pattern_website+'|'+pattern_email+'|'+pattern_fraction+'|'+pattern_decimal_numbers+'|'+pattern_symbol +'|'+pattern_word , text)


    string = u'masche'
    print re.findall(regex['Jugador-Javier_Mascherano'], string, re.I)

    #if (re.match(pattern_messi,'meso', re.I|re.U)):
    #    print 'Match'
    #else:
    #   print 'Not Match'

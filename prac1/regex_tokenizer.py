#!/usr/bin/env python
# -*- coding: utf-8 -*-

#
# Iván Sánchez Rodríguez
# ALC - MIARFID  2017
#

import re
import argparse

def parse_args():
    parser = argparse.ArgumentParser(description='Tokenizes the text within a text file. Iván Sánchez Rodríguez. ALC  -  MIARFID 2017')
    parser.add_argument('input_path', metavar='input',
                        help='relative path to the input text file')
    parser.add_argument('output_path', metavar='output', const='output.txt', nargs='?',
                        help='relative path to the output text file. If there is no output specified, the result will be printed instead of saved')


    return parser.parse_args()


if __name__ == "__main__":
    args = parse_args()

    with open(args.input_path, 'r') as myfile:
        text=myfile.read()



    pattern_decimal_numbers = '\d*[.,]?\d+'

    pattern_date = '\d{2}[-|/]\d{2}[-|/]\d{4}|\d{2}[-|/]\d{2}'

    pattern_time = '\d{1,2}:\d{2}'

    pattern_website = 'http[s]?://\w*.\w*.\w*[.\w*]*[/[\w~]*]*'

    pattern_email = '[\w]*\@[\w]*.[\w]*.[\w]*'

    pattern_fraction = '\d+/\d+'

    pattern_symbol = '\?|"|¿|¡|!|,|\.\.\.|\.|;|:|\'|%'
    pattern_word = '[\wÁÉÍÓÚáéíóúñ]*[-[\wÁÉÍÓÚáéíóúñ]+]?'


    tokens = re.findall(pattern_date+'|'+pattern_time+'|'+pattern_website+'|'+pattern_email+'|'+pattern_fraction+'|'+pattern_decimal_numbers+'|'+pattern_symbol +'|'+pattern_word , text)


    if(args.output_path == None):
        for token in tokens:
            print token
    else:
        f = open(args.output_path, 'w')
        for token in tokens:
            f.write(token+'\n')
        f.close()
